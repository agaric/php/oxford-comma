<?php

namespace Agaric\OxfordComma;

/**
 * Make an array of items into a proper, punctuated, and sentence-ready list.
 *
 * Based on www.drupaler.co.uk/blog/oxford-comma/503
 * Grammatically fun helper to make a list of things in a sentence, i.e.
 * turn an array into a string 'a, b, and c'.
 *
 * If your software supports translations, please pass in the translated string
 * for 'and', 'or', or whatever your conjunction is.  For example, in Drupal:
 *
 *   // Translate 'and' and 'or'.
 *   if ($type == 'and') {
 *     $type = t('and', [], ['context' => 'Final join']);
 *   }
 *   elseif ($type == 'or') {
 *     $type = t('or', [], ['context' => 'Final join']);
 *   }
 *
 * Then pass it in like this: `oxford_comma_list($list, ['type' => $type]);`
 *
 * @param $list
 *   An array of words or items to join.
 * @param $settings
 *   An array of optional settings to use in making the Oxford comma list:
 *   - join
 *     The join for the list. Defaults to a comma followed by a space.
 *     To make an Oxford comma list with semicolons, use '; '.
 *   - final_join
 *     The text to use between the last two items. Defaults to 'and'. Pass in
 *     'or' and 'and' without translation; translate any other join.
 *   - oxford
 *     Use the join together with the final join between the final items.
 *     Change this from default 'TRUE' and you are a philistine.
 */
function oxford_comma_list($list, $settings = []) {
  if (dusankasan_knapsack_every_value($list, 'is_scalar')) {
    return list_strings($list, $settings);
  }
  else {
    return list_things($list, $settings);
  }
}

/**
 * @param $list
 *   An array of strings, integers, or other simple string-convertible types.
 * @param array $settings
 *   @see oxford_comma_list().
 *
 * If your list is scalar (strings, numbers, booleans etc) your join and final
 * join must be scalar too.
 *
 * @return array
 */
function list_strings($list, $settings = []) {

  // If we're handed an empty list, return an empty string.
  if (count($list) === 0) {
    return '';
  }

  // If the array of items passed in has only one item in it, return that item.
  if (count($list) === 1) {
    return current($list);
  }

  // We trust extract here because we sanitize input in our settings function.
  // Gives us the variables $join and $final_join.
  extract(_settings($list, $settings));

  // Take the last two elements off of the $list array.
  $final = array_splice($list, -2, 2);

  // Combine the final two removed elements around the final join string.
  $final_string = implode($final_join, $final);
  // Add the combined elements (now a single element) back onto the list array.
  array_push($list, $final_string);
  // Return the list as a text string joined together with commas (or other).
  return implode($join, $list);
}

/**
 * @param $list
 *   An array of objects, array items, or resources not convertible to strings.
 * @param array $settings
 *
 * @return array
 * @see oxford_comma_list().
 *
 */
function list_things($list, $settings = []) {

  $processed_list = [];

  if (count($list) === 0) {
    return $processed_list;
  }

  // We have at least one item, add it.
  $processed_list[] = current($list);

  // If the array of items passed in has only one item in it, return that item.
  if (count($list) === 1) {
    return $processed_list;
  }

  // We trust extract here because we sanitize input in our settings function.
  // Gives us the variables $join and $final_join.
  extract(_settings($list, $settings, FALSE));

  // Take the last element off of the $list array.
  $final = array_splice($list, -1, 1);

  // Put the join between elements in the first part of the list.  Note that
  // this loop will not and should not run if there are fewer than three items.
  $len = count($list) - 1;
  // Note that ++$i is slightly faster but works the same as $i++ here.
  for ($i = 0; $i < $len; ++$i) {
    $processed_list[] = $join;
    $processed_list[] = next($list);
  }

  // Add the final join array or string and the final removed element.
  if (is_array($final_join)) {
    foreach ($final_join as $part) {
      $processed_list[] = $part;
    }
  }
  else {
    $processed_list[] = $final_join;
  }
  $processed_list[] = current($final);

  return $processed_list;
}

/**
 * Helper function to set up our settings.
 */
function _settings($list, $settings = [], $scalar = TRUE) {
  // Set default settings.
  $join = ', ';
  $final_join = 'and';
  $space = ' ';
  $first_and_final_join = ' and ';
  $oxford = TRUE;
  // Overwrite default settings with any passed-in settings that apply.
  extract($settings, EXTR_IF_EXISTS);

  if ($oxford && count($list) > 2) {
    if ($scalar) {
      $final_join = $join . $final_join . $space;
    }
    else {
      $final_join = [$join, $final_join];
      // Generally we expect non-scalar final joins to provide their own spacing
      // but we allow them to hand in a non-scalar (array, object) spacer if
      // they so desire.
      if (!is_scalar($space)) {
        $final_join[] = $space;
      }
    }
  }
  else {
    if ($scalar) {
      $final_join = $space . $final_join . $space;
    }
    else {
      // We recommend non-scalar lists provide a special option for when there
      // are only two items: "this and that" rather than "this, that, and these"
      // because in the two-item case the join needs a preceeding space.
      if (!is_scalar($first_and_final_join)) {
        $final_join = $first_and_final_join;
      }
      elseif (!is_scalar($space)) {
        $final_join[] = $space;
      }
      // Keep the output of final join consistently an array so we can foreach it.
      $final_join = [$final_join];
    }
  }

  return [
    'join' => $join,
    'final_join' => $final_join,
    'oxford' => $oxford,
  ];
}

/**
 * From the DusanKasan/Knapsack PHP library for working with collections.
 * https://github.com/DusanKasan/Knapsack/blob/master/src/collection_functions.php
 *
 * Based on \DusanKasan\Knapsack\every().
 *
 * We've actually modified it to take only a value, or else we get warnings
 * using is_scalar as the callable function (expects only one value passed in).
 *
 * If we use more than one function, we'll require the library instead.
 *
 * Returns true if $function returns true for every item in $collection
 *
 * @param array|Traversable $collection
 * @param callable $function ($value, $key)
 *
 * @return bool
 */
function dusankasan_knapsack_every_value($collection, callable $function) {
  foreach ($collection as $value) {
    if (!$function($value)) {
      return FALSE;
    }
  }
  return TRUE;
}
