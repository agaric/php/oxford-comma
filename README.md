# Agaric Oxford Comma Readme

## Usage

"We talked with his teachers, Oprah, and Barack Obama" *not* "We talked with his teachers, Oprah and Barack Obama". 

"The dog ate carrots, grapes, and peanut butter and jelly sandwiches" *not* "The dog ate carrots, grapes and peanut butter and jelly sandwiches."

## Usage of this library

Turn an array of strings into a natural-language list:

```
use Agaric\OxfordComma;

$array = ["State Fair", "South Pacific", "The King and I"];
$text = OxfordComma\oxford_comma_list($array); // State Fair, South Pacific, and The King and I
```
